Checkout component:

We manage state of component by order id that are created on server side and have to be managed by client and passed in consecutive requests

There are 3 endpoint provided:

1) POST /initShoping - creates new order (cart) and returns order id to the client

2) POST /product/{order_id} - need to provide id(name) of the product in request body and adds that product to order that id needs to be also provided in Path parameter. Total order price including discounts are calculated on the fly and returns as a result of tat request

3) GET /product/{order_id} - returns total receipt as json with all listed items,amount and price and additional "Discount" position with all discount values.

Design

1) Use org.joda.Money class to represent monetary values since double is not convenient for that purpose and java BigDecimal is also too heavy

2) All Prices are in USD as Default currency. In more complex application currency may differ depends on clint location for instance.

3) I use different classes to calculates discounts of just added product. It is used to follow OCP. If requirements changes 
and some other discounts is added then we need just write new class that implements DiscountProvider class and "register" it in Discount CalculatorProvider without touching existing code.That will make code more flexible and less error-prone.

4) Adding product endpoint returns String instead of Money object to have better control over format that is returned to client. In that case we can use our custom format and we are not depend on third party library

5) I create separate DTO object to send to client as receipt response to not be dependent on domain objects too much. Now he have better control what is return to client. That code may be redundant,but it will save much time in the future.

6) Created interceptor to check orderId instead move it to application logic. Will return 404 code if no order found.

7) Component will return 404 if product with non-existing id is passed. This is handled by application logic since interception post body is not elegant. I use mapping certain exceptions to http codes on Spring.

8) Classes are not thread-safe. We assume that consecutive request for adding product needs to wait after before one is finished so no race conditions will occurs. But since order and order line objects are mutable proper synchronization or  other techniques may be needed to achieve thread-safety

9) Component uses in-memory database so all stored information about orders will be deleted after application shutdown.

Application:

1) Application is written using spring boot

2) Application uses Spring MVC to implement rest endpoint.

3) Application uses java 8 features.

4) Application uses maven to build and executes.

Tests:

1) To make sure that application works correctly I write unit tests for most important classes. I do not write UT for Repositories or domain objects since it was very simple.

2) OrderManagerTest is not "purely" unit test b/c it uses real discount calculators to test most important functionality of calculation discounts

3) ApplicationSpecification test are written in spring boot test. It tested full flow woth creating new order adding products with discounts and printing receipt.

Running Application

1) There are two maven profiles avaliable. Default one will execute all tests even system tests. This is not convenient since that tests may execute long so there is "skip-system-tests" profile

2) Application can be run using "mvn spring-boot:run". It will start embedded server on localhost using defalt http port

hej tu Anita
ja tu kommituje ciekawe rzeczy
poniedzialek i wtorek