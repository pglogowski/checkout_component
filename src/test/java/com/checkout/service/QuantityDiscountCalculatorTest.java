package com.checkout.service;


import com.checkout.domain.Order;
import com.checkout.domain.OrderLine;
import com.checkout.domain.Product;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

/**
 * Created by pglogowski on 11/08/17.
 */
public class QuantityDiscountCalculatorTest {

    private QuantityDiscountCalculator quantityDiscountCalculator = new QuantityDiscountCalculator();

    @Test
    public void testCalculateDiscountForExactSpecialQuantity() {
        Product p1 = new Product("Samsung S6", Money.of(CurrencyUnit.USD,2.0d), Money.of(CurrencyUnit.USD,1.87d),3);

        OrderLine ol1 = new OrderLine(p1);
        ol1.addProduct();
        ol1.addProduct();
        ol1.addProduct();

        Order order = Mockito.mock(Order.class);


        Money discount = quantityDiscountCalculator.calculateDiscount(order, ol1);

        Assert.assertEquals("Calculated discount do not match", Money.of(CurrencyUnit.USD, 0.39d), discount);
    }

    @Test
    public void testCalculateDiscountForTwiceSpecialQuantity() {
        Product p1 = new Product("Samsung S6", Money.of(CurrencyUnit.USD,2.0d), Money.of(CurrencyUnit.USD,1.85d),3);

        OrderLine ol1 = new OrderLine(p1);
        ol1.addProduct();
        ol1.addProduct();
        ol1.addProduct();
        ol1.addProduct();
        ol1.addProduct();
        ol1.addProduct();

        Order order = Mockito.mock(Order.class);


        Money discount = quantityDiscountCalculator.calculateDiscount(order, ol1);

        Assert.assertEquals("Calculated discount do not match", Money.of(CurrencyUnit.USD, 0.45d), discount);
    }

    @Test
    public void testCalculateDiscountForSlightyGraterQuantity() {
        Product p1 = new Product("Samsung S6", Money.of(CurrencyUnit.USD,2.0d), Money.of(CurrencyUnit.USD,1.83d),2);

        OrderLine ol1 = new OrderLine(p1);
        ol1.addProduct();
        ol1.addProduct();
        ol1.addProduct();

        Order order = Mockito.mock(Order.class);


        Money discount = quantityDiscountCalculator.calculateDiscount(order, ol1);

        Assert.assertEquals("Calculated discount do not match", Money.of(CurrencyUnit.USD, 0.0d), discount);

    }

    @Test
    public void testCalculateDiscountForHighlyGreaterQuantityButNotApplcable() {
        Product p1 = new Product("Samsung S6", Money.of(CurrencyUnit.USD,2.0d), Money.of(CurrencyUnit.USD,1.81d),2);

        OrderLine ol1 = new OrderLine(p1);
        ol1.addProduct();
        ol1.addProduct();
        ol1.addProduct();
        ol1.addProduct();
        ol1.addProduct();
        ol1.addProduct();
        ol1.addProduct();

        Order order = Mockito.mock(Order.class);


        Money discount = quantityDiscountCalculator.calculateDiscount(order, ol1);

        Assert.assertEquals("Calculated discount do not match", Money.of(CurrencyUnit.USD, 0.0d), discount);
    }

    @Test
    public void testCalculateDiscountLowerQuantity() {
        Product p1 = new Product("Samsung S6", Money.of(CurrencyUnit.USD,2.0d), Money.of(CurrencyUnit.USD,1.83d),2);

        OrderLine ol1 = new OrderLine(p1);
        ol1.addProduct();

        Order order = Mockito.mock(Order.class);


        Money discount = quantityDiscountCalculator.calculateDiscount(order, ol1);

        Assert.assertEquals("Calculated discount do not match", Money.of(CurrencyUnit.USD, 0.0d), discount);
    }

}
