package com.checkout.service;

import com.checkout.domain.Order;
import com.checkout.domain.Product;
import com.checkout.domain.ProductComboDiscount;
import com.checkout.repository.OrderRepository;
import com.checkout.repository.ProductRepository;
import com.checkout.repository.ProductComboDiscountRepository;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/**
 * Created by pglogowski on 11/08/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class OrderManagerAddProductToOrderTest {

    private static final String PRODUCT_ID_1 = "ID1";
    private static final String PRODUCT_ID_2 = "ID2";
    private static final String PRODUCT_ID_3 = "ID3";
    private static final String PRODUCT_ID_4 = "ID4";

    private static final Money PRODUCT_ID_1_PRICE = Money.of(CurrencyUnit.USD, 1.0d);
    private static final Money PRODUCT_ID_2_PRICE = Money.of(CurrencyUnit.USD, 2.0d);
    private static final Money PRODUCT_ID_3_PRICE = Money.of(CurrencyUnit.USD, 3.0d);
    private static final Money PRODUCT_ID_4_PRICE = Money.of(CurrencyUnit.USD, 4.0d);

    private static final Money PRODUCT_ID_1_SPECIAL_PRICE = Money.of(CurrencyUnit.USD, 0.50d);
    private static final Money PRODUCT_ID_2_SPECIAL_PRICE = Money.of(CurrencyUnit.USD, 1.75d);
    private static final Money PRODUCT_ID_3_SPECIAL_PRICE = Money.of(CurrencyUnit.USD, 2.8d);
    private static final Money PRODUCT_ID_4_SPECIAL_PRICE = Money.of(CurrencyUnit.USD, 3.95d);

    private static final int PRODUCT_1_SPECIAL_AMOUNT = 2;
    private static final int PRODUCT_2_SPECIAL_AMOUNT = 3;
    private static final int PRODUCT_3_SPECIAL_AMOUNT = 5;
    private static final int PRODUCT_4_SPECIAL_AMOUNT = 10;

    private static final String TESTED_ORDER_ID = "OID";

    private static final Product p1 = new Product(PRODUCT_ID_1, PRODUCT_ID_1_PRICE, PRODUCT_ID_1_SPECIAL_PRICE, PRODUCT_1_SPECIAL_AMOUNT);
    private static final Product p2 = new Product(PRODUCT_ID_2, PRODUCT_ID_2_PRICE, PRODUCT_ID_2_SPECIAL_PRICE, PRODUCT_2_SPECIAL_AMOUNT);
    private static final Product p3 = new Product(PRODUCT_ID_3, PRODUCT_ID_3_PRICE, PRODUCT_ID_3_SPECIAL_PRICE, PRODUCT_3_SPECIAL_AMOUNT);
    private static final Product p4 = new Product(PRODUCT_ID_4, PRODUCT_ID_4_PRICE, PRODUCT_ID_4_SPECIAL_PRICE, PRODUCT_4_SPECIAL_AMOUNT);

    @Mock
    private OrderRepository orderRepository;

    @Mock
    private ProductRepository productRepository;

    @Mock
    private DiscountCalculatorProvider provider;

    @InjectMocks
    private OrderManager underTest = new DefaultOrderManager();

    // For  Discount calculator
    @Mock
    private ProductComboDiscountRepository discountRepository;

    @Spy
    @InjectMocks
    private ProductsDiscountCalculator productsDiscountCalculator = new ProductsDiscountCalculator();

    @Spy
    private QuantityDiscountCalculator quantityCalculator = new QuantityDiscountCalculator();

    @Before
    public void setUp() {
        Mockito.when(productRepository.get(PRODUCT_ID_1)).thenReturn(p1);
        Mockito.when(productRepository.get(PRODUCT_ID_2)).thenReturn(p2);
        Mockito.when(productRepository.get(PRODUCT_ID_3)).thenReturn(p3);
        Mockito.when(productRepository.get(PRODUCT_ID_4)).thenReturn(p4);

        Mockito.when(productRepository.exists(PRODUCT_ID_1)).thenReturn(true);
        Mockito.when(productRepository.exists(PRODUCT_ID_2)).thenReturn(true);
        Mockito.when(productRepository.exists(PRODUCT_ID_3)).thenReturn(true);
        Mockito.when(productRepository.exists(PRODUCT_ID_4)).thenReturn(true);

        Mockito.when(orderRepository.get(TESTED_ORDER_ID)).thenReturn(new Order());

        Mockito.when(provider.getDiscountCalculators())
                .thenReturn(new HashSet<>(Arrays.asList(quantityCalculator, productsDiscountCalculator)));
    }

    @Test
    public void testAddingNewProductAndCheckDiscountQuantityDiscount() {
        Mockito.when(discountRepository.exists(Mockito.anyString())).thenReturn(false);

        Money noDiscountProduct = underTest.addProductToOrder(TESTED_ORDER_ID, PRODUCT_ID_1);

        Assert.assertEquals("There should be no discount after add first product", PRODUCT_ID_1_PRICE, noDiscountProduct);

        // Add first product for 2 quantity discount - means add 4 products
        Money wholeOrderExpectedPrice = PRODUCT_ID_1_PRICE.plus(PRODUCT_ID_1_PRICE).minus(p1.getQuantityDiscount().multipliedBy(p1.getSpecialPriceAmount()));
        Assert.assertEquals("There should be quantity discount after add second product of that type", wholeOrderExpectedPrice
                , underTest.addProductToOrder(TESTED_ORDER_ID, PRODUCT_ID_1));

        wholeOrderExpectedPrice = wholeOrderExpectedPrice.plus(PRODUCT_ID_1_PRICE);
        Assert.assertEquals("There should be no quantity discount after add third product of that type", wholeOrderExpectedPrice
                , underTest.addProductToOrder(TESTED_ORDER_ID, PRODUCT_ID_1));

        wholeOrderExpectedPrice = wholeOrderExpectedPrice.plus(PRODUCT_ID_1_PRICE).minus(p1.getQuantityDiscount().multipliedBy(p1.getSpecialPriceAmount()));
        Assert.assertEquals("There should be  quantity discount after add fourth product of that type", wholeOrderExpectedPrice
                , underTest.addProductToOrder(TESTED_ORDER_ID, PRODUCT_ID_1));

        // Add second product for one quantity discount - means add 4 products to see if quantity counts only one
        wholeOrderExpectedPrice = wholeOrderExpectedPrice.plus(PRODUCT_ID_2_PRICE);
        Assert.assertEquals("There should be no quantity discount after add first product of that type", wholeOrderExpectedPrice
                , underTest.addProductToOrder(TESTED_ORDER_ID, PRODUCT_ID_2));

        wholeOrderExpectedPrice = wholeOrderExpectedPrice.plus(PRODUCT_ID_2_PRICE);
        Assert.assertEquals("There should be quantity discount after add second product of that type", wholeOrderExpectedPrice
                , underTest.addProductToOrder(TESTED_ORDER_ID, PRODUCT_ID_2));

        wholeOrderExpectedPrice = wholeOrderExpectedPrice.plus(PRODUCT_ID_2_PRICE).minus(p2.getQuantityDiscount().multipliedBy(p2.getSpecialPriceAmount()));
        Assert.assertEquals("There should be quantity discount after add second product of that type", wholeOrderExpectedPrice
                , underTest.addProductToOrder(TESTED_ORDER_ID, PRODUCT_ID_2));

        wholeOrderExpectedPrice = wholeOrderExpectedPrice.plus(PRODUCT_ID_2_PRICE);
        Assert.assertEquals("There should be quantity discount after add second product of that type", wholeOrderExpectedPrice
                , underTest.addProductToOrder(TESTED_ORDER_ID, PRODUCT_ID_2));
    }

    @Test
    public void testAddingNewProductAndCheckProductComboDiscount() {
        Map<String, Money> p4Discounts = new HashMap<>();
        p4Discounts.put(p3.getName(), Money.of(CurrencyUnit.USD,0.1d));
        p4Discounts.put(p2.getName(), Money.of(CurrencyUnit.USD,0.2d));
        ProductComboDiscount p4Discount = new ProductComboDiscount(p4.getName(), p4Discounts);

        Map<String, Money> p3Discounts = new HashMap<>();
        p3Discounts.put(p4.getName(), Money.of(CurrencyUnit.USD,0.1d));
        p3Discounts.put(p2.getName(), Money.of(CurrencyUnit.USD,0.3d));
        ProductComboDiscount p3Discount = new ProductComboDiscount(p3.getName(), p3Discounts);

        Map<String, Money> p2Discounts = new HashMap<>();
        p2Discounts.put(p4.getName(), Money.of(CurrencyUnit.USD,0.2d));
        p2Discounts.put(p3.getName(), Money.of(CurrencyUnit.USD,0.3d));
        ProductComboDiscount p2Discount = new ProductComboDiscount(p2.getName(), p2Discounts);

        Mockito.when(discountRepository.exists(p1.getName())).thenReturn(false);
        Mockito.when(discountRepository.exists(p2.getName())).thenReturn(true);
        Mockito.when(discountRepository.exists(p3.getName())).thenReturn(true);
        Mockito.when(discountRepository.exists(p4.getName())).thenReturn(true);

        Mockito.when(discountRepository.get(p2.getName())).thenReturn(p2Discount);
        Mockito.when(discountRepository.get(p3.getName())).thenReturn(p3Discount);
        Mockito.when(discountRepository.get(p4.getName())).thenReturn(p4Discount);

        Money wholeOrderExpectedPrice = Money.of(PRODUCT_ID_4_PRICE);
        Assert.assertEquals("There should be no discount after add first product", wholeOrderExpectedPrice, underTest.addProductToOrder(TESTED_ORDER_ID, PRODUCT_ID_4));

        wholeOrderExpectedPrice = wholeOrderExpectedPrice.plus(PRODUCT_ID_3_PRICE).minus(0.1d);
        Assert.assertEquals("There should be discount after add combo product", wholeOrderExpectedPrice, underTest.addProductToOrder(TESTED_ORDER_ID, PRODUCT_ID_3));

        wholeOrderExpectedPrice = wholeOrderExpectedPrice.plus(PRODUCT_ID_4_PRICE);
        Assert.assertEquals("There should be discount after add combo product", wholeOrderExpectedPrice, underTest.addProductToOrder(TESTED_ORDER_ID, PRODUCT_ID_4));

        wholeOrderExpectedPrice = wholeOrderExpectedPrice.plus(PRODUCT_ID_2_PRICE).minus(0.2d).minus(0.3d);
        Assert.assertEquals("There should be discount after add combo product", wholeOrderExpectedPrice, underTest.addProductToOrder(TESTED_ORDER_ID, PRODUCT_ID_2));

        wholeOrderExpectedPrice = wholeOrderExpectedPrice.plus(PRODUCT_ID_3_PRICE).minus(0.1d);
        Assert.assertEquals("There should be discount after add combo product", wholeOrderExpectedPrice, underTest.addProductToOrder(TESTED_ORDER_ID, PRODUCT_ID_3));

        wholeOrderExpectedPrice = wholeOrderExpectedPrice.plus(PRODUCT_ID_3_PRICE);
        Assert.assertEquals("There should be discount after add combo product", wholeOrderExpectedPrice, underTest.addProductToOrder(TESTED_ORDER_ID, PRODUCT_ID_3));

        wholeOrderExpectedPrice = wholeOrderExpectedPrice.plus(PRODUCT_ID_1_PRICE);
        Assert.assertEquals("There should be discount after add combo product", wholeOrderExpectedPrice, underTest.addProductToOrder(TESTED_ORDER_ID, PRODUCT_ID_1));
    }

    @Test
    public void testAddingNewProductAndCheckProductComboDiscountAndQuantityDiscount() {
        Map<String, Money> p1Discounts = new HashMap<>();
        p1Discounts.put(p2.getName(), Money.of(CurrencyUnit.USD,0.1d));
        p1Discounts.put(p3.getName(), Money.of(CurrencyUnit.USD,0.2d));
        ProductComboDiscount p1Discount = new ProductComboDiscount(p1.getName(), p1Discounts);

        Map<String, Money> p2Discounts = new HashMap<>();
        p2Discounts.put(p1.getName(), Money.of(CurrencyUnit.USD,0.1d));
        p2Discounts.put(p3.getName(), Money.of(CurrencyUnit.USD,0.3d));
        ProductComboDiscount p2Discount = new ProductComboDiscount(p2.getName(), p2Discounts);

        Map<String, Money> p3Discounts = new HashMap<>();
        p3Discounts.put(p1.getName(), Money.of(CurrencyUnit.USD,0.2d));
        p3Discounts.put(p2.getName(), Money.of(CurrencyUnit.USD,0.3d));
        ProductComboDiscount p3Discount = new ProductComboDiscount(p3.getName(), p3Discounts);


        Mockito.when(discountRepository.exists(p1.getName())).thenReturn(true);
        Mockito.when(discountRepository.exists(p2.getName())).thenReturn(true);
        Mockito.when(discountRepository.exists(p3.getName())).thenReturn(true);
        Mockito.when(discountRepository.exists(p4.getName())).thenReturn(false);

        Mockito.when(discountRepository.get(p1.getName())).thenReturn(p1Discount);
        Mockito.when(discountRepository.get(p2.getName())).thenReturn(p2Discount);
        Mockito.when(discountRepository.get(p3.getName())).thenReturn(p3Discount);

        Money wholeOrderExpectedPrice = Money.of(PRODUCT_ID_1_PRICE);
        Assert.assertEquals("There should be no discount after add first product", wholeOrderExpectedPrice, underTest.addProductToOrder(TESTED_ORDER_ID, PRODUCT_ID_1));

        wholeOrderExpectedPrice = wholeOrderExpectedPrice.plus(PRODUCT_ID_2_PRICE).minus(0.1d);
        Assert.assertEquals("There should be discount after add combo product", wholeOrderExpectedPrice, underTest.addProductToOrder(TESTED_ORDER_ID, PRODUCT_ID_2));

        wholeOrderExpectedPrice = wholeOrderExpectedPrice.plus(PRODUCT_ID_2_PRICE);
        Assert.assertEquals("There should be discount after add combo product", wholeOrderExpectedPrice, underTest.addProductToOrder(TESTED_ORDER_ID, PRODUCT_ID_2));

        wholeOrderExpectedPrice = wholeOrderExpectedPrice.plus(PRODUCT_ID_1_PRICE).minus(0.1d).minus(PRODUCT_ID_1_SPECIAL_PRICE.multipliedBy(PRODUCT_1_SPECIAL_AMOUNT));
        Assert.assertEquals("There should be discount after add combo product", wholeOrderExpectedPrice, underTest.addProductToOrder(TESTED_ORDER_ID, PRODUCT_ID_1));

        wholeOrderExpectedPrice = wholeOrderExpectedPrice.plus(PRODUCT_ID_3_PRICE).minus(0.2d).minus(0.3d);
        Assert.assertEquals("There should be discount after add combo product", wholeOrderExpectedPrice, underTest.addProductToOrder(TESTED_ORDER_ID, PRODUCT_ID_3));

        wholeOrderExpectedPrice = wholeOrderExpectedPrice.plus(PRODUCT_ID_4_PRICE);
        Assert.assertEquals("There should be discount after add combo product", wholeOrderExpectedPrice, underTest.addProductToOrder(TESTED_ORDER_ID, PRODUCT_ID_4));

        wholeOrderExpectedPrice = wholeOrderExpectedPrice.plus(PRODUCT_ID_4_PRICE);
        Assert.assertEquals("There should be discount after add combo product", wholeOrderExpectedPrice, underTest.addProductToOrder(TESTED_ORDER_ID, PRODUCT_ID_4));
    }

}
