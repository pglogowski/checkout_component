package com.checkout.service;

import com.checkout.domain.Order;
import com.checkout.domain.OrderLine;
import com.checkout.domain.Product;
import com.checkout.repository.OrderRepository;
import com.checkout.repository.ProductComboDiscountRepository;
import com.checkout.repository.ProductRepository;
import com.checkout.rest.responses.OrderLineDTO;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class OrderManagerGetRecepitTest {

    private static final String PRODUCT_ID_1 = "ID1";

    private static final Money PRODUCT_ID_1_PRICE = Money.of(CurrencyUnit.USD, 1.0d);

    private static final Money PRODUCT_ID_1_SPECIAL_PRICE = Money.of(CurrencyUnit.USD, 0.50d);

    private static final int PRODUCT_1_SPECIAL_AMOUNT = 2;

    private static final String TESTED_ORDER_ID = "OID";

    private static final Product p1 = new Product(PRODUCT_ID_1, PRODUCT_ID_1_PRICE, PRODUCT_ID_1_SPECIAL_PRICE, PRODUCT_1_SPECIAL_AMOUNT);

    @Mock
    private OrderRepository orderRepository;

    @Mock
    private ProductRepository productRepository;

    @Mock
    private DiscountCalculatorProvider provider;

    @InjectMocks
    private OrderManager underTest = new DefaultOrderManager();

    // For  Discount calculator
    @Mock
    private ProductComboDiscountRepository discountRepository;

    @Spy
    @InjectMocks
    private ProductsDiscountCalculator productsDiscountCalculator = new ProductsDiscountCalculator();

    @Spy
    private QuantityDiscountCalculator quantityCalculator = new QuantityDiscountCalculator();

    @Before
    public void setUp() {
        Mockito.when(discountRepository.exists(Mockito.anyString())).thenReturn(false);
        Mockito.when(productRepository.get(PRODUCT_ID_1)).thenReturn(p1);

        Mockito.when(productRepository.exists(PRODUCT_ID_1)).thenReturn(true);

        Mockito.when(orderRepository.get(TESTED_ORDER_ID)).thenReturn(new Order());

        Mockito.when(provider.getDiscountCalculators())
                .thenReturn(new HashSet<>(Arrays.asList(quantityCalculator, productsDiscountCalculator)));
    }

    @Test
    public void tesGetRecepitWithDiscount() {
        // Given - two product with id1 to apply discount
        underTest.addProductToOrder(TESTED_ORDER_ID, PRODUCT_ID_1);
        underTest.addProductToOrder(TESTED_ORDER_ID, PRODUCT_ID_1);

        Order finalOrderRecepit = underTest.getOrderForAllRecepit(TESTED_ORDER_ID);

        List<OrderLine> orderLines = finalOrderRecepit.getOrderLines();
        Assert.assertEquals("Number of order lines do not match !", 2, orderLines.size());
        checkOrderLine(orderLines.get(0), PRODUCT_ID_1, 2, Money.of(CurrencyUnit.USD, 2.0d));
        checkOrderLine(orderLines.get(1), "Discounts", 1, Money.of(CurrencyUnit.USD, -1.0d));
    }

    @Test
    public void testGetRecepitWithoutDiscount() {
        // Given - two product with id1 to not apply discount
        underTest.addProductToOrder(TESTED_ORDER_ID, PRODUCT_ID_1);

        Order finalOrderRecepit = underTest.getOrderForAllRecepit(TESTED_ORDER_ID);

        List<OrderLine> orderLines = finalOrderRecepit.getOrderLines();
        Assert.assertEquals("Number of order lines do not match !", 1, orderLines.size());
        checkOrderLine(orderLines.get(0), PRODUCT_ID_1, 1, Money.of(CurrencyUnit.USD, 1.0d));
    }

    private void checkOrderLine(OrderLine orderLine, String productName, int amount, Money price) {
        Assert.assertEquals("Product name do not match", productName, orderLine.getProduct().getName());
        Assert.assertEquals("Product name do not match", amount, orderLine.getAmount());
        Assert.assertEquals("Product name do not match", price, orderLine.getPrice());

    }

}
