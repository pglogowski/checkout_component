package com.checkout.service;

import com.checkout.domain.Order;
import com.checkout.domain.OrderLine;
import com.checkout.domain.Product;
import com.checkout.domain.ProductComboDiscount;
import com.checkout.repository.ProductComboDiscountRepository;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;

@RunWith(MockitoJUnitRunner.class)
public class ProductsDiscountCalculatorTest {

    @Mock
    private ProductComboDiscountRepository repository;

    @InjectMocks
    private ProductsDiscountCalculator underTest;

    @Test
    public void testCalculateProcessorForOnePairOfProductsAndNoOtherPromoProductsInOrder() {
        Product p1 = new Product("Samsung S6", Money.of(CurrencyUnit.USD,2.0d), Money.of(CurrencyUnit.USD,1.0d),10);
        Product p2 = new Product("Samsung S7", Money.of(CurrencyUnit.USD,3.0d), Money.of(CurrencyUnit.USD,2.0d),20);

        OrderLine itemToAdd = new OrderLine(p2);
        addProductToOrderLine(itemToAdd, 3);


        Order order = new Order();
        addProductsToOrder(order, p1, 8);

        Map<String, Money> map = new HashMap<>();
        map.put(p1.getName(), Money.of(CurrencyUnit.USD, 1.0d));
        ProductComboDiscount discount = new ProductComboDiscount(p2.getName(), map);

        Mockito.when(repository.exists(p2.getName())).thenReturn(true);
        Mockito.when(repository.get(p2.getName())).thenReturn(discount);

        // Add 4th product to order and see if discounts were calculated properties
        // We expect to have discount with combo p2 -- p1 only
        itemToAdd.addProduct();
        Assert.assertEquals("", Money.of(CurrencyUnit.USD, 1.0d), underTest.calculateDiscount(order, itemToAdd));

    }
    @Test
    public void testCalculateProcessorForSeveralPairOfProductsAndOtherPromoProductsInOrder() {
        Product p1 = new Product("Samsung S6", Money.of(CurrencyUnit.USD,2.0d), Money.of(CurrencyUnit.USD,1.0d),10);
        Product p2 = new Product("Samsung S7", Money.of(CurrencyUnit.USD,3.0d), Money.of(CurrencyUnit.USD,2.0d),20);
        Product p3 = new Product("Samsung S7 Edge", Money.of(CurrencyUnit.USD,4.1d), Money.of(CurrencyUnit.USD,2.0d),20);
        Product p4 = new Product("Samsung S8", Money.of(CurrencyUnit.USD,4.3d), Money.of(CurrencyUnit.USD,2.0d),20);

        OrderLine itemToAdd = new OrderLine(p2);
        addProductToOrderLine(itemToAdd, 2);

        Order order = new Order();
        addProductsToOrder(order, p1, 3);
        addProductsToOrder(order, p3, 3);
        addProductsToOrder(order, p4, 2);


        // 1 USD Discount when  buy p2 with p1
        // 0.75 USD Discount when buy p2 with p3
        Map<String, Money> map = new HashMap<>();
        map.put(p1.getName(), Money.of(CurrencyUnit.USD, 0.5d));
        map.put(p3.getName(), Money.of(CurrencyUnit.USD, 0.75d));
        map.put(p4.getName(), Money.of(CurrencyUnit.USD, 0.25d));
        ProductComboDiscount discount = new ProductComboDiscount(p2.getName(), map);


        Mockito.when(repository.exists(p2.getName())).thenReturn(true);
        Mockito.when(repository.get(p2.getName())).thenReturn(discount);

        Mockito.when(repository.exists(p2.getName())).thenReturn(true);
        Mockito.when(repository.get(p2.getName())).thenReturn(discount);

        // Add 3th product to order and see if discounts were calculated properties
        // We expect to have discount with combo p2 -- p1 and p2 -- p3
        itemToAdd.addProduct();
        Assert.assertEquals("", Money.of(CurrencyUnit.USD, 1.25d), underTest.calculateDiscount(order, itemToAdd));
    }

    @Test
    public void testCalculateProcessorForSeveralPairOfProductsAndNoOtherPromoProductsInOrder() {
        Product p1 = new Product("Samsung S6", Money.of(CurrencyUnit.USD,2.0d), Money.of(CurrencyUnit.USD,1.0d),10);
        Product p2 = new Product("Samsung S7", Money.of(CurrencyUnit.USD,3.0d), Money.of(CurrencyUnit.USD,2.0d),20);
        Product p3 = new Product("Samsung S7 Edge", Money.of(CurrencyUnit.USD,4.1d), Money.of(CurrencyUnit.USD,2.0d),20);

        OrderLine itemToAdd = new OrderLine(p2);
        addProductToOrderLine(itemToAdd, 2);

        Order order = new Order();
        addProductsToOrder(order, p1, 3);
        addProductsToOrder(order, p3, 3);


        // 1 USD Discount when  buy p2 with p1
        // 0.75 USD Discount when buy p2 with p3
        Map<String, Money> map = new HashMap<>();
        map.put(p1.getName(), Money.of(CurrencyUnit.USD, 0.5d));
        map.put(p3.getName(), Money.of(CurrencyUnit.USD, 0.75d));
        ProductComboDiscount discount = new ProductComboDiscount(p2.getName(), map);


        Mockito.when(repository.exists(p2.getName())).thenReturn(true);
        Mockito.when(repository.get(p2.getName())).thenReturn(discount);

        Mockito.when(repository.exists(p2.getName())).thenReturn(true);
        Mockito.when(repository.get(p2.getName())).thenReturn(discount);

        // Add 3th product to order and see if discounts were calculated properties
        // We expect to have discount with combo p2 -- p1 and p2 -- p3
        itemToAdd.addProduct();
        Assert.assertEquals("", Money.of(CurrencyUnit.USD, 1.25d), underTest.calculateDiscount(order, itemToAdd));
    }

    @Test
    public void testCalculateDiscountWhenOnePairOfProductsWithOtherPromoProductAreAlsoInOrder() {
        Product p1 = new Product("Samsung S6", Money.of(CurrencyUnit.USD,2.0d), Money.of(CurrencyUnit.USD,1.0d),10);
        Product p2 = new Product("Samsung S7", Money.of(CurrencyUnit.USD,3.0d), Money.of(CurrencyUnit.USD,2.0d),20);
        Product p3 = new Product("Samsung S7 Edge", Money.of(CurrencyUnit.USD,4.1d), Money.of(CurrencyUnit.USD,2.0d),20);
        Product p4 = new Product("Samsung S8", Money.of(CurrencyUnit.USD,4.3d), Money.of(CurrencyUnit.USD,2.0d),20);


        OrderLine itemToAdd = new OrderLine(p2);
        addProductToOrderLine(itemToAdd, 3);

        Order order = new Order();
        addProductsToOrder(order, p1, 4);
        addProductsToOrder(order, p3, 2);
        addProductsToOrder(order, p4, 1);

        // 1 USD Discount when  buy p2 with p1
        // 1.5 USD Discount when buy p2 with p3
        Map<String, Money> map = new HashMap<>();
        map.put(p1.getName(), Money.of(CurrencyUnit.USD, 1.0d));
        map.put(p3.getName(), Money.of(CurrencyUnit.USD, 1.5d));
        ProductComboDiscount discount = new ProductComboDiscount(p2.getName(), map);

        // 1 USD Discount when  buy p4 with p3
        map = new HashMap<>();
        map.put(p3.getName(), Money.of(CurrencyUnit.USD, 1.0d));
        ProductComboDiscount discount1 = new ProductComboDiscount(p4.getName(), map);

        Mockito.when(repository.exists(p4.getName())).thenReturn(true);
        Mockito.when(repository.get(p4.getName())).thenReturn(discount1);

        Mockito.when(repository.exists(p2.getName())).thenReturn(true);
        Mockito.when(repository.get(p2.getName())).thenReturn(discount);

        // Add 4th product to order and see if discounts were calculated properties
        // We expect to have discount with combo p2 -- p1 only
        itemToAdd.addProduct();
        Assert.assertEquals("", Money.of(CurrencyUnit.USD, 1.0d), underTest.calculateDiscount(order, itemToAdd));

    }

    private void addProductsToOrder(final Order o, final Product p1, final int quantity) {
        IntStream.range(0, quantity).forEach(i->o.addProduct(p1));
    }

    private void addProductToOrderLine(final OrderLine ol, int times) {
        IntStream.range(0, times).forEach(i->ol.addProduct());
    }

    @Test
    public void testCalculateIfThereIsNoDiscountForProduct() {
        Product p1 = new Product("Samsung S6", Money.of(CurrencyUnit.USD,2.0d), Money.of(CurrencyUnit.USD,1.0d),10);
        Product p2 = new Product("Samsung S7", Money.of(CurrencyUnit.USD,3.0d), Money.of(CurrencyUnit.USD,2.0d),20);

        OrderLine ol2 = new OrderLine(p2);

        Order order = new Order();
        order.addProduct(p1);

        Mockito.when(repository.get(p2.getName())).thenReturn(null);

        Assert.assertEquals("", Money.of(CurrencyUnit.USD, 0.0d), underTest.calculateDiscount(order, ol2));
    }

    @Test
    public void testCalculateIfThereIsNoDiscountForProductInOrder() {
        Product p1 = new Product("Samsung S6", Money.of(CurrencyUnit.USD,2.0d), Money.of(CurrencyUnit.USD,1.0d),10);
        Product p2 = new Product("Samsung S7", Money.of(CurrencyUnit.USD,3.0d), Money.of(CurrencyUnit.USD,2.0d),20);

        OrderLine ol2 = new OrderLine(p2);

        Order order = new Order();
        order.addProduct(p1);

        Map<String, Money> map = new HashMap<>();
        map.put(p1.getName(), Money.of(CurrencyUnit.USD, 1.0d));
        ProductComboDiscount firstDiscountForProductNotInOrder = new ProductComboDiscount("NO_BONUS_1", map);
        ProductComboDiscount secondDiscountForProductNotInOrder = new ProductComboDiscount("NO_BONUS_2", map);
        Mockito.when(repository.get("NO_BONUS")).thenReturn(firstDiscountForProductNotInOrder);
        Mockito.when(repository.get("NO_BONUS_2")).thenReturn(secondDiscountForProductNotInOrder);

        underTest.calculateDiscount(order, ol2);

        Assert.assertEquals("", Money.of(CurrencyUnit.USD, 0.0d), underTest.calculateDiscount(order, ol2));
    }

}
