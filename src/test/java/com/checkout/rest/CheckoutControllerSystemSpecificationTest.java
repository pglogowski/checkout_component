package com.checkout.rest;

import com.checkout.rest.responses.OrderLineDTO;
import com.checkout.rest.responses.ReceiptResponse;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CheckoutControllerSystemSpecificationTest {

    private static final String PRODUCT_ID_1 = "ID1";
    private static final String PRODUCT_ID_2 = "ID2";
    private static final String PRODUCT_ID_3 = "ID3";
    private static final String PRODUCT_ID_4 = "ID4";

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void testInitShoppingCartIntegration() {
        ResponseEntity<String> responseEntity =
                restTemplate.postForEntity("/initShoping", null, String.class);
        String orderId = responseEntity.getBody();

        Assert.assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        Assert.assertNotNull(orderId);
    }

    @Test
    public void testInitShopingAddSomeProductsAndPrintRecepit() {
        ResponseEntity<String> responseEntity =
                restTemplate.postForEntity("/initShoping", null, String.class);
        String orderId = responseEntity.getBody();

        Assert.assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        Assert.assertNotNull(orderId);

        // Add "ID1" product - do not expect discount
        ResponseEntity<String> wholeOrderPriceResponse = getAddProductResponse(orderId, PRODUCT_ID_1);
        checkPriceForWholeOrder(wholeOrderPriceResponse, Money.of(CurrencyUnit.USD, 1.0d));

        // Add "ID2" product - expect combo discount 0.1 USD
        wholeOrderPriceResponse = getAddProductResponse(orderId, PRODUCT_ID_2);
        checkPriceForWholeOrder(wholeOrderPriceResponse, Money.of(CurrencyUnit.USD, 2.9d));

        // Add "ID2" product - do not expect discount
        wholeOrderPriceResponse = getAddProductResponse(orderId, PRODUCT_ID_2);
        checkPriceForWholeOrder(wholeOrderPriceResponse, Money.of(CurrencyUnit.USD, 4.9d));

        // Add "ID1" product - expecting discount for quantity and combo
        wholeOrderPriceResponse = getAddProductResponse(orderId, PRODUCT_ID_1);
        checkPriceForWholeOrder(wholeOrderPriceResponse, Money.of(CurrencyUnit.USD, 4.8d));

        // Add "ID3" product - expect discount for combo with 2 products
        wholeOrderPriceResponse = getAddProductResponse(orderId, PRODUCT_ID_3);
        checkPriceForWholeOrder(wholeOrderPriceResponse, Money.of(CurrencyUnit.USD, 7.3d));

        // Add "ID4" product - do not expect discount
        wholeOrderPriceResponse = getAddProductResponse(orderId, PRODUCT_ID_4);
        checkPriceForWholeOrder(wholeOrderPriceResponse, Money.of(CurrencyUnit.USD, 11.3d));

        // Add "ID4" product - do not expect discount
        wholeOrderPriceResponse = getAddProductResponse(orderId, PRODUCT_ID_4);
        checkPriceForWholeOrder(wholeOrderPriceResponse, Money.of(CurrencyUnit.USD, 15.3d));

        ReceiptResponse recepitResponse = restTemplate.getForObject("/product/" + orderId, ReceiptResponse.class);

        Money expectedPriceInRecepit = Money.parse(recepitResponse.getPrice());
        Assert.assertEquals("Expected order price do not match the actual", expectedPriceInRecepit, Money.of(CurrencyUnit.USD, 15.3d));

        List<OrderLineDTO> orderLines = recepitResponse.getOrderLines();

        Assert.assertEquals("Should be exactly 5 order lines", 5, orderLines.size());
        checkOrderLine(orderLines.get(0), PRODUCT_ID_1, 2, Money.of(CurrencyUnit.USD, 2.0d));
        checkOrderLine(orderLines.get(1), PRODUCT_ID_2, 2, Money.of(CurrencyUnit.USD, 4.0d));
        checkOrderLine(orderLines.get(2), PRODUCT_ID_3, 1, Money.of(CurrencyUnit.USD, 3.0d));
        checkOrderLine(orderLines.get(3), PRODUCT_ID_4, 2, Money.of(CurrencyUnit.USD, 8.0d));
        checkOrderLine(orderLines.get(4), "Discounts", 1, Money.of(CurrencyUnit.USD, -1.7d));
    }

    @Test
    public void testAddProductForNonExistingOrder() {
        ResponseEntity<String> wholeOrderPriceResponse = restTemplate.postForEntity("/product/NO_EXISTING_ORDER", new HttpEntity<String>("dummy"), String.class);
        Assert.assertEquals(HttpStatus.NOT_FOUND, wholeOrderPriceResponse.getStatusCode());
    }

    @Test
    public void testAddProductForNonExistingProduct() {
        ResponseEntity<String> responseEntity =
                restTemplate.postForEntity("/initShoping", null, String.class);
        String orderId = responseEntity.getBody();

        ResponseEntity<String> wholeOrderPriceResponse = restTemplate.postForEntity("/product/"+orderId, new HttpEntity<String>("NON_EXISITNG"), String.class);
        Assert.assertEquals(HttpStatus.NOT_FOUND, wholeOrderPriceResponse.getStatusCode());
    }

    private ResponseEntity<String> getAddProductResponse(final String orderId, final String productId) {
        HttpEntity<String> request = new HttpEntity<>(productId);
        ResponseEntity<String> wholeOrderPriceResponse = restTemplate.postForEntity("/product/" + orderId, request, String.class);
        Assert.assertEquals(HttpStatus.OK, wholeOrderPriceResponse.getStatusCode());

        return wholeOrderPriceResponse;
    }

    private void checkOrderLine(OrderLineDTO orderLineDTO, String productName, int amount, Money price) {
        Assert.assertEquals("Product name do not match", productName, orderLineDTO.getProductName());
        Assert.assertEquals("Product name do not match", amount, orderLineDTO.getAmount().intValue());
        Assert.assertEquals("Product name do not match", price, Money.parse(orderLineDTO.getPrice()));

    }

    private void checkPriceForWholeOrder(ResponseEntity<String> wholeOrderPriceResponse, Money expectedPrice) {
        String wholeOrderPriceAsMoneyAsString = wholeOrderPriceResponse.getBody();
        Money wholeOrderPriceAsMoney = Money.parse(wholeOrderPriceAsMoneyAsString);

        Assert.assertEquals("Expected order price do not match the actual", expectedPrice, wholeOrderPriceAsMoney);
    }

}
