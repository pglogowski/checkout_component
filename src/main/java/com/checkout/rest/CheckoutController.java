package com.checkout.rest;

import com.checkout.domain.Order;
import com.checkout.domain.OrderLine;
import com.checkout.rest.responses.ReceiptResponse;
import com.checkout.service.OrderManager;
import org.joda.money.Money;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by pglogowski on 11/08/17.
 */
@RestController
public class CheckoutController {

    @Autowired
    private OrderManager orderManager;

    @RequestMapping(value = "/initShoping", method = RequestMethod.POST)
    public ResponseEntity<String> initShoppingCart() {
        String orderId = orderManager.createNewOrder();
        return new ResponseEntity<>(orderId, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/product/{orderId}", method = RequestMethod.POST)
    ResponseEntity<String> scanProduct(@PathVariable String orderId, @RequestBody String productId) {
        Money productPrice = orderManager.addProductToOrder(orderId, productId);
        return new ResponseEntity<>(productPrice.toString(), HttpStatus.OK);
    }

    @RequestMapping(value = "/product/{orderId}", method = RequestMethod.GET, produces = "application/json")
    ResponseEntity<ReceiptResponse> getFinalReceipt(@PathVariable String orderId) {
        Order finalOrder = orderManager.getOrderForAllRecepit(orderId);
        return new ResponseEntity<>(new ReceiptResponse(finalOrder.getOrderLines(), finalOrder.getTotalPrice()), HttpStatus.OK);
    }

}
