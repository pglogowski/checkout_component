package com.checkout.rest.responses;

import com.checkout.domain.OrderLine;
import org.joda.money.Money;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by pglogowski on 11/08/17.
 */
public class ReceiptResponse {

    private List<OrderLineDTO> orderLines;

    private String price;

    public ReceiptResponse() {
        // to satisfy Jackson specification
    }

    public ReceiptResponse(List<OrderLine> orderLines, Money price) {
        this.orderLines = orderLines.stream().map(OrderLineDTO::new).collect(Collectors.toList());
        this.price = price.toString();
    }

    public List<OrderLineDTO> getOrderLines() {
        return orderLines;
    }

    public void setOrderLines(List<OrderLineDTO> orderLines) {
        this.orderLines = orderLines;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
