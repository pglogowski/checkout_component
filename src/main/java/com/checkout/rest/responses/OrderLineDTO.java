package com.checkout.rest.responses;

import com.checkout.domain.OrderLine;

public class OrderLineDTO {

    private String productName;

    private Integer amount;

    private String price;

    public OrderLineDTO() {
        // To satisfy jackson specification
    }

    public OrderLineDTO(OrderLine orderLine) {
        this.productName = orderLine.getProduct().getName();
        this.amount = orderLine.getAmount();
        this.price = orderLine.getPrice().toString();
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OrderLineDTO that = (OrderLineDTO) o;

        if (productName != null ? !productName.equals(that.productName) : that.productName != null) return false;
        if (amount != null ? !amount.equals(that.amount) : that.amount != null) return false;
        return price != null ? price.equals(that.price) : that.price == null;
    }

    @Override
    public int hashCode() {
        int result = productName != null ? productName.hashCode() : 0;
        result = 31 * result + (amount != null ? amount.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        return result;
    }
}
