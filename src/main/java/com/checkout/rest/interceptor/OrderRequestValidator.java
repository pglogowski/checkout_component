package com.checkout.rest.interceptor;

import com.checkout.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class OrderRequestValidator extends HandlerInterceptorAdapter {

    @Autowired
    private OrderRepository orderRepository;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String orderId = resolveOrderId(request);
        if (!orderRepository.exists(orderId)) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            return false;
        }

        return true;
    }

    private boolean isAddProductRequest(HttpServletRequest request) {
        return "POST".equalsIgnoreCase(request.getMethod());
    }

    private String resolveOrderId(HttpServletRequest request) {
        String[] servletPathParts = request.getServletPath().split("/");
        String orderId = servletPathParts[servletPathParts.length - 1];

        return orderId;
    }
}
