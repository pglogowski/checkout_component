package com.checkout.service;

import com.checkout.domain.Order;
import com.checkout.domain.OrderLine;
import org.joda.money.Money;

public interface DiscountCalculator {

    Money calculateDiscount(Order order, OrderLine orderLine);

}
