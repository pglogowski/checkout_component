package com.checkout.service;

import com.checkout.domain.Order;
import com.checkout.domain.OrderLine;
import com.checkout.domain.Product;
import com.checkout.repository.OrderRepository;
import com.checkout.repository.ProductRepository;
import com.checkout.service.exception.NoSuchProductException;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Set;

/**
 * Created by pglogowski on 10/08/17.
 */
@Component
public class DefaultOrderManager implements OrderManager {

    private static final Money ZERO = Money.of(CurrencyUnit.USD, 0.0d);

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private DiscountCalculatorProvider provider;

    public Money addProductToOrder(String orderId, String productId) {
        if (!productRepository.exists(productId)) {
            throw new NoSuchProductException("There is no product with id" + productId);
        }
        final Order order = orderRepository.get(orderId);
        final Product product = productRepository.get(productId);

        final OrderLine productOrderLine = order.addProduct(product);

        Set<DiscountCalculator> discountCalculators = provider.getDiscountCalculators();

        Money totalDiscount = discountCalculators.stream()
                .map(c -> c.calculateDiscount(order, productOrderLine))
                .reduce(Money.of(CurrencyUnit.USD, 0.0d), Money::plus);

        order.applayDiscount(totalDiscount);

        return order.getTotalPrice();
    }

    @Override
    public Order getOrderForAllRecepit(String orderId) {
        Order order = orderRepository.get(orderId);
        if (order.hasDiscounts()) {
            order.addDiscountOrderLine();
        }

        return order;
    }


    @Override
    public String createNewOrder() {
        Order newOrder = new Order();
        orderRepository.add(newOrder);

        return newOrder.getOrderId();
    }

}
