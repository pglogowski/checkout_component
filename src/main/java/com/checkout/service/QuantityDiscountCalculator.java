package com.checkout.service;

import com.checkout.domain.Order;
import com.checkout.domain.OrderLine;
import com.checkout.domain.Product;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;
import org.springframework.stereotype.Component;

@Component("QuantityDiscountCalculator")
public class QuantityDiscountCalculator implements DiscountCalculator {


    @Override
    public Money calculateDiscount(Order order, OrderLine orderLine) {
        Product product = orderLine.getProduct();
        int amount = orderLine.getAmount();
        int specialPriceAmount = orderLine.getProduct().getSpecialPriceAmount();

        boolean applyDiscount = amount%specialPriceAmount==0;

        return applyDiscount ? product.getQuantityDiscount().multipliedBy(product.getSpecialPriceAmount())
                : Money.of(CurrencyUnit.USD, 0.0d);
    }
}
