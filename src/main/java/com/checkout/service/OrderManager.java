package com.checkout.service;

import com.checkout.domain.Order;
import com.checkout.domain.OrderLine;
import org.joda.money.Money;

import java.util.List;

/**
 * Created by pglogowski on 11/08/17.
 */
public interface OrderManager {

    Money addProductToOrder(String orderId, String productId);

    String createNewOrder();

    Order getOrderForAllRecepit(String orderId);
}
