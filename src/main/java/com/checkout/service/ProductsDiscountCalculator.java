package com.checkout.service;

import com.checkout.domain.Order;
import com.checkout.domain.OrderLine;
import com.checkout.domain.ProductComboDiscount;
import com.checkout.repository.ProductComboDiscountRepository;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component("ProductsDiscountCalculator")
public class ProductsDiscountCalculator implements DiscountCalculator {

    private static final Money ZERO = Money.of(CurrencyUnit.USD, 0.0d);

    @Autowired
    private ProductComboDiscountRepository discountRepository;

    public ProductsDiscountCalculator() {

    }

    @Override
    public Money calculateDiscount(Order order, final OrderLine justAddedProduct) {
        final String productId = justAddedProduct.getProduct().getName();
        if(!discountRepository.exists(productId)) {
            return ZERO;
        }
        ProductComboDiscount discountedProductToAdd = discountRepository.get(productId);
        Set<String> discountedProductForProductToAdd = discountedProductToAdd.getRelatedProductDiscounts().keySet();

        List<OrderLine> productAlreadyInOrderToDiscount = order.getOrderLines().stream()
                .filter(ol -> discountedProductForProductToAdd.contains(ol.getProduct().getName()))
                .collect(Collectors.toList());

        Money totalDiscount = productAlreadyInOrderToDiscount.stream().map(ol ->
                {
                    boolean discountApplicable = ol.getAmount() >= justAddedProduct.getAmount();
                    return discountApplicable ? discountedProductToAdd.getDiscountForProduct(ol.getProduct().getName()) : ZERO;
                }
        ).reduce(ZERO, Money::plus);

        return totalDiscount;
    }
}
