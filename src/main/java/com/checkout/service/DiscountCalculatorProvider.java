package com.checkout.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by pglogowski on 11/08/17.
 */
@Component
public class DiscountCalculatorProvider {


    @Autowired
    @Qualifier("QuantityDiscountCalculator")
    private DiscountCalculator quantityDiscountCalculator;

    @Autowired
    @Qualifier("ProductsDiscountCalculator")
    private DiscountCalculator productsDiscountCalculator;


    public Set<DiscountCalculator> getDiscountCalculators() {
        return new HashSet<>(Arrays.asList(quantityDiscountCalculator, productsDiscountCalculator));
    }

}
