package com.checkout.service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "No such product")
public class NoSuchProductException extends  RuntimeException {

    public NoSuchProductException(String message) {
        super(message);
    }
}
