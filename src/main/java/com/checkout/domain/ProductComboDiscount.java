package com.checkout.domain;

import org.joda.money.Money;

import java.util.Map;

/**
 * Created by pglogowski on 10/08/17.
 */
public class ProductComboDiscount {

    private final String productId;

    private final Map<String, Money> relatedProductDiscounts;


    public ProductComboDiscount(String productId, Map<String, Money> relatedProductDiscounts) {
        this.productId = productId;
        this.relatedProductDiscounts = relatedProductDiscounts;
    }

    public Money getDiscountForProduct(String productId) {
        return relatedProductDiscounts.get(productId);
    }

    public String getProductId() {
        return productId;
    }

    public Map<String, Money> getRelatedProductDiscounts() {
        return relatedProductDiscounts;
    }
}
