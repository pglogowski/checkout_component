package com.checkout.domain;

import org.joda.money.Money;

/**
 * Created by checkout on 10/08/17.
 */
public class Product {

    private final String name;

    private final Money price;

    private final Money specialPrice;

    private final int specialPriceAmount;


    public Product(String name, Money price, Money specialPrice, int specialPriceAmount) {
        this.name = name;
        this.price = price;
        this.specialPrice = specialPrice;
        this.specialPriceAmount = specialPriceAmount;
    }

    public String getName() {
        return name;
    }

    public Money getPrice() {
        return price;
    }

    public Money getSpecialPrice() {
        return specialPrice;
    }

    public int getSpecialPriceAmount() {
        return specialPriceAmount;
    }

    public Money getQuantityDiscount() {
        return getPrice().minus(getSpecialPrice());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Product product = (Product) o;

        return name.equals(product.name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }


}
