package com.checkout.domain;

import org.joda.money.CurrencyUnit;
import org.joda.money.Money;

import java.util.*;

/**
 * Created by pglogowski on 10/08/17.
 */
public class Order {

    private static final Money ZERO = Money.of(CurrencyUnit.USD, 0.0d);

    private static final String DISCOUNT_ORDER_LINE_NAME = "Discounts";

    private final String orderId;

    private final Map<String, OrderLine> orderLines;

    private Money totalPrice;

    private Money totalDiscounts;

    public Order() {
        this.orderId = UUID.randomUUID().toString();
        this.orderLines = new LinkedHashMap<>();
        this.totalPrice = Money.of(CurrencyUnit.USD, 0.0d);
        this.totalDiscounts = Money.of(CurrencyUnit.USD, 0.0d);
    }

    public List<OrderLine> getOrderLines() {
        return new ArrayList<>(orderLines.values());
    }

    public Money getTotalPrice() {
        return totalPrice;
    }

    public Money getTotalDiscounts() {
        return totalDiscounts;
    }

    public void applayDiscount(Money discount) {
        totalPrice = totalPrice.minus(discount);
        totalDiscounts = totalDiscounts.minus(discount);
    }

    public String getOrderId() {
        return orderId;
    }

    public OrderLine addProduct(Product product) {
        OrderLine orderLineForProduct = orderLines.getOrDefault(product.getName(), new OrderLine(product));
        orderLineForProduct.addProduct();
        orderLines.put(product.getName(), orderLineForProduct);
        totalPrice = totalPrice.plus(product.getPrice());
        return orderLineForProduct;
    }

    public void addDiscountOrderLine() {
        Product discountProduct = new Product(DISCOUNT_ORDER_LINE_NAME, totalDiscounts, Money.of(CurrencyUnit.USD, 0.0d), 0);
        OrderLine discountOrderLine = new OrderLine(discountProduct);
        discountOrderLine.addProduct();
        orderLines.put(discountProduct.getName(), discountOrderLine);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Order order = (Order) o;

        return orderId.equals(order.orderId);
    }

    @Override
    public int hashCode() {
        return orderId.hashCode();
    }

    public boolean hasDiscounts() {
        return !ZERO.equals(getTotalDiscounts());
    }
}
