package com.checkout.domain;

import org.joda.money.CurrencyUnit;
import org.joda.money.Money;

/**
 * Created by pglogowski on 10/08/17.
 */
public class OrderLine {

    private final Product product;

    private int amount = 0;

    private Money price;

    public OrderLine(Product product) {
        this.product = product;
        this.price = Money.of(CurrencyUnit.USD, 0.0d);
    }


    public Product getProduct() {
        return product;
    }

    public int getAmount() {
        return amount;
    }

    public Money getPrice() {
        return price;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OrderLine orderLine = (OrderLine) o;

        if (amount != orderLine.amount) return false;
        if (!product.equals(orderLine.product)) return false;
        return price.equals(orderLine.price);
    }

    @Override
    public int hashCode() {
        int result = product.hashCode();
        result = 31 * result + amount;
        result = 31 * result + price.hashCode();
        return result;
    }

    public void addProduct() {
        amount++;
        price = product.getPrice().multipliedBy(amount);
    }
}
