package com.checkout;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by pglogowski on 11/08/17.
 */
@SpringBootApplication
public class CheckoutServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(CheckoutServerApplication.class, args);
    }

}
