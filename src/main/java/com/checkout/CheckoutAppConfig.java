package com.checkout;

import com.checkout.rest.interceptor.OrderRequestValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Created by pglogowski on 11/08/17.
 */
@Configuration
public class CheckoutAppConfig extends WebMvcConfigurerAdapter {

    @Autowired
    private OrderRequestValidator requestValidator;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(requestValidator).addPathPatterns("/product/*");
    }


}
