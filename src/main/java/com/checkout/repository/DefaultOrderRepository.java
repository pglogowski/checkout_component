package com.checkout.repository;

import com.checkout.domain.Order;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by pglogowski on 10/08/17.
 */
@Repository
public class DefaultOrderRepository implements OrderRepository {

    private Map<String, Order> orders = new HashMap<>();

    public boolean add(Order element) {
        orders.put(element.getOrderId(), element);
        return true;
    }

    public Order get(String id) {
        return orders.get(id);
    }

    public boolean exists(String id) {
        return orders.containsKey(id);
    }
}
