package com.checkout.repository;

/**
 * Created by pglogowski on 10/08/17.
 */
public interface BaseRepository<K, T> {

    boolean add(T element);

    T get(K id);

    boolean exists(K id);

}
