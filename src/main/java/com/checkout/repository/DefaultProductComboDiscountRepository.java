package com.checkout.repository;

import com.checkout.domain.ProductComboDiscount;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class DefaultProductComboDiscountRepository implements ProductComboDiscountRepository {

    private static final String PRODUCT_ID_1 = "ID1";
    private static final String PRODUCT_ID_2 = "ID2";
    private static final String PRODUCT_ID_3 = "ID3";
    private static final String PRODUCT_ID_4 = "ID4";

    private final Map<String, ProductComboDiscount> discounts;

    {
        discounts = new HashMap<>();

        Map<String, Money> p1Discounts = new HashMap<>();
        p1Discounts.put(PRODUCT_ID_2, Money.of(CurrencyUnit.USD,0.1d));
        p1Discounts.put(PRODUCT_ID_3, Money.of(CurrencyUnit.USD,0.2d));
        ProductComboDiscount p1Discount = new ProductComboDiscount(PRODUCT_ID_1, p1Discounts);
        discounts.put(p1Discount.getProductId(), p1Discount);

        Map<String, Money> p2Discounts = new HashMap<>();
        p2Discounts.put(PRODUCT_ID_1, Money.of(CurrencyUnit.USD,0.1d));
        p2Discounts.put(PRODUCT_ID_3, Money.of(CurrencyUnit.USD,0.3d));
        ProductComboDiscount p2Discount = new ProductComboDiscount(PRODUCT_ID_2, p2Discounts);
        discounts.put(p2Discount.getProductId(), p2Discount);

        Map<String, Money> p3Discounts = new HashMap<>();
        p3Discounts.put(PRODUCT_ID_1, Money.of(CurrencyUnit.USD,0.2d));
        p3Discounts.put(PRODUCT_ID_2, Money.of(CurrencyUnit.USD,0.3d));
        ProductComboDiscount p3Discount = new ProductComboDiscount(PRODUCT_ID_3, p3Discounts);
        discounts.put(p3Discount.getProductId(), p3Discount);
    }

    @Override
    public boolean add(ProductComboDiscount element) {
        discounts.put(element.getProductId(), element);
        return true;
    }

    @Override
    public ProductComboDiscount get(String id) {
        return discounts.get(id);
    }

    @Override
    public boolean exists(String id) {
        return discounts.containsKey(id);
    }
}
