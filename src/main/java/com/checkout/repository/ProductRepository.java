package com.checkout.repository;

import com.checkout.domain.Product;

/**
 * Created by pglogowski on 10/08/17.
 */
public interface ProductRepository extends BaseRepository<String, Product> {
}
