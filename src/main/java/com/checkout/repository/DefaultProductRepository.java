package com.checkout.repository;

import com.checkout.domain.Product;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by pglogowski on 10/08/17.
 */
@Repository
public class DefaultProductRepository implements ProductRepository {

    private static final String PRODUCT_ID_1 = "ID1";
    private static final String PRODUCT_ID_2 = "ID2";
    private static final String PRODUCT_ID_3 = "ID3";
    private static final String PRODUCT_ID_4 = "ID4";

    private static final Money PRODUCT_ID_1_PRICE = Money.of(CurrencyUnit.USD, 1.0d);
    private static final Money PRODUCT_ID_2_PRICE = Money.of(CurrencyUnit.USD, 2.0d);
    private static final Money PRODUCT_ID_3_PRICE = Money.of(CurrencyUnit.USD, 3.0d);
    private static final Money PRODUCT_ID_4_PRICE = Money.of(CurrencyUnit.USD, 4.0d);

    private static final Money PRODUCT_ID_1_SPECIAL_PRICE = Money.of(CurrencyUnit.USD, 0.50d);
    private static final Money PRODUCT_ID_2_SPECIAL_PRICE = Money.of(CurrencyUnit.USD, 1.75d);
    private static final Money PRODUCT_ID_3_SPECIAL_PRICE = Money.of(CurrencyUnit.USD, 2.8d);
    private static final Money PRODUCT_ID_4_SPECIAL_PRICE = Money.of(CurrencyUnit.USD, 3.95d);

    private static final int PRODUCT_1_SPECIAL_AMOUNT = 2;
    private static final int PRODUCT_2_SPECIAL_AMOUNT = 3;
    private static final int PRODUCT_3_SPECIAL_AMOUNT = 5;
    private static final int PRODUCT_4_SPECIAL_AMOUNT = 10;

    private final Map<String, Product> products;

    {
        products = new HashMap<>();
        Product p1 = new Product(PRODUCT_ID_1, PRODUCT_ID_1_PRICE, PRODUCT_ID_1_SPECIAL_PRICE, PRODUCT_1_SPECIAL_AMOUNT);
        Product p2 = new Product(PRODUCT_ID_2, PRODUCT_ID_2_PRICE, PRODUCT_ID_2_SPECIAL_PRICE, PRODUCT_2_SPECIAL_AMOUNT);
        Product p3 = new Product(PRODUCT_ID_3, PRODUCT_ID_3_PRICE, PRODUCT_ID_3_SPECIAL_PRICE, PRODUCT_3_SPECIAL_AMOUNT);
        Product p4 = new Product(PRODUCT_ID_4, PRODUCT_ID_4_PRICE, PRODUCT_ID_4_SPECIAL_PRICE, PRODUCT_4_SPECIAL_AMOUNT);
        products.put(p1.getName(), p1);
        products.put(p2.getName(), p2);
        products.put(p3.getName(), p3);
        products.put(p4.getName(), p4);
    }

    @Override
    public boolean add(Product element) {
        products.put(element.getName(), element);
        return true;
    }

    @Override
    public Product get(String id) {
        return products.get(id);
    }

    @Override
    public boolean exists(String id) {
        return products.containsKey(id);
    }
}
