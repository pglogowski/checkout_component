package com.checkout.repository;

import com.checkout.domain.Order;

/**
 * Created by pglogowski on 10/08/17.
 */
public interface OrderRepository extends BaseRepository<String, Order> {
}
