package com.checkout.repository;

import com.checkout.domain.ProductComboDiscount;

public interface ProductComboDiscountRepository extends BaseRepository<String, ProductComboDiscount> {


}
